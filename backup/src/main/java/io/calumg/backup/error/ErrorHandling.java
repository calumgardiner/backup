package io.calumg.backup.error;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class ErrorHandling {

	public static void displayErrorDialog(String errorHeader, Exception e) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle("Error");
		alert.setHeaderText(errorHeader);
		alert.setContentText(e.getMessage());
		alert.showAndWait();
	}

}
