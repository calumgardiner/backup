package io.calumg.backup.file;

import io.calumg.backup.file.checksum.FileHashUtils;
import io.calumg.backup.file.checksum.Hashable;
import io.calumg.backup.file.exception.InvalidRelativePathException;
import io.calumg.backup.file.relative.RelativeFile;
import io.calumg.backup.file.selected.Selectable;

import java.io.File;
import java.io.IOException;

public class BackupFile extends RelativeFile implements Hashable, Selectable {

	private static final long serialVersionUID = -3199050155401704270L;

	private Boolean selected = true;
	private BackupStatus status = BackupStatus.NEW_FILE;

	public BackupFile(String pathname, String relativePath) throws InvalidRelativePathException {
		super(pathname, relativePath);
	}

	public BackupFile(File file, File relativePath) throws InvalidRelativePathException {
		super(file, relativePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.calumg.backup.file.checksum.Hashable#getHexDigest()
	 */
	@Override
	public String getHexDigest() throws IOException {
		return FileHashUtils.getSHA1DigestForFile(this);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.calumg.backup.file.checksum.Hashable#compareHash(io.calumg.backup.
	 * file.checksum.Hashable)
	 */
	@Override
	public Boolean compareHash(Hashable object) throws IOException {
		return this.getHexDigest().equals(object.getHexDigest());
	}

	@Override
	public Boolean isSelected() {
		return this.selected;
	}

	@Override
	public void setSelected(Boolean selected) {
		this.selected = selected;
	}

	public BackupStatus getStatus() {
		return status;
	}

	public void setStatus(BackupStatus status) {
		this.status = status;
	}

}
