package io.calumg.backup.file.relative;

import io.calumg.backup.file.exception.InvalidRelativePathException;

import java.io.File;

/**
 * <p>
 * File with implementation of {@link RelativeFilePath}, describing a file or
 * directory with some relative file path.
 * </p>
 * 
 * @author calum
 *
 */
public class RelativeFile extends File implements RelativeFilePath {

	private static final long serialVersionUID = 5800691105958418171L;
	private File relativeFile;

	public RelativeFile(String pathname, String relativePath) throws InvalidRelativePathException {
		super(pathname);
		setRelativeFilePath(new File(relativePath));
	}

	public RelativeFile(File file, File relativePath) throws InvalidRelativePathException {
		super(file.getAbsolutePath());
		setRelativeFilePath(relativePath);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see io.calumg.backup.file.RelativeFilePath#getRelativeFilePath()
	 */
	public File getRelativeFilePath() {
		return this.relativeFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * io.calumg.backup.file.RelativeFilePath#setRelativeFilePath(java.io.File)
	 */
	public void setRelativeFilePath(File path) throws InvalidRelativePathException {
		if (!path.exists())
			throw new InvalidRelativePathException("The relative path set does not exist.");
		if (!path.isDirectory())
			throw new InvalidRelativePathException("The relative path set is not a directory");
		if (!validateRelativePath(path))
			throw new InvalidRelativePathException("The file " + this.getAbsolutePath()
					+ " cannot have a relative path " + path.getAbsolutePath());
		this.relativeFile = path;
	}

	/**
	 * Checks whether its actually possible to have this relative path for this
	 * file.
	 * 
	 * @return true if the relative path given makes sense.
	 */
	private Boolean validateRelativePath(File path) {
		return this.getAbsolutePath().startsWith(path.getAbsolutePath());
	}

	@Override
	public String getPathRelative() {
		return this.getAbsolutePath().replaceFirst(this.relativeFile.getAbsolutePath(), "");
	}

}
