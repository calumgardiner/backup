package io.calumg.backup.file;

public enum BackupStatus {

	NEW_FILE, CHANGES_MADE, NO_CHANGE, FILE_REMOVED;

}
