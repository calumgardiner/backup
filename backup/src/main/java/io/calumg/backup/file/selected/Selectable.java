package io.calumg.backup.file.selected;

public interface Selectable {

	public Boolean isSelected();

	public void setSelected(Boolean selected);
}
