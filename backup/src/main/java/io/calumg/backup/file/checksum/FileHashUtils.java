package io.calumg.backup.file.checksum;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;

public class FileHashUtils {

	public static String getSHA1DigestForFile(File file) throws IOException {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
			byte[] data = IOUtils.toByteArray(fis);
			return DigestUtils.sha1Hex(data);
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		} finally {
			IOUtils.closeQuietly(fis);
		}
	}

}
