package io.calumg.backup.file.exception;

import io.calumg.backup.file.relative.RelativeFilePath;

/**
 * <p>
 * InvalidRelativePathException, thrown when something went wrong setting the
 * relative path from the interface {@link RelativeFilePath}. For example if the
 * relative path set is not a directory or doesn't exist.
 * </p>
 * 
 * @author calum
 *
 */
public class InvalidRelativePathException extends Exception {

	private static final long serialVersionUID = 5430020136943938499L;

	public InvalidRelativePathException(String message) {
		super(message);
	}

}
