package io.calumg.backup.file.checksum;

import java.io.IOException;

public interface Hashable {

	public String getHexDigest() throws IOException;

	public Boolean compareHash(Hashable object) throws IOException;

}
