package io.calumg.backup.file.relative;

import java.io.File;

/**
 * <p>
 * Classes which implement this interface should have some path which can be
 * described as there relative path, i.e. the file './files/text.txt' could have
 * the relative path '/home/calum/', meaning the actual path of the file would
 * be '/home/calum/files/text.txt'. Having this relative path should allow
 * similar file structures to be compared while disregarding their actual full
 * path, only taking into account everything beyond their relative path.
 * </p>
 * 
 * @author calum
 *
 */
public interface RelativeFilePath {

	/**
	 * Get the relative file path of this element.
	 * 
	 * @return the relative file path
	 */
	public File getRelativeFilePath();

	/**
	 * Set the relative file path of this element.
	 * 
	 * @param path
	 */
	public void setRelativeFilePath(File path) throws Exception;

	/**
	 * Get the file path relative to the relative file path.<br>
	 * e.g. /home/calum/files/file with relative file path /home/calum/ would
	 * return /files/file
	 * 
	 * @return the file path relative to the relative path.
	 */
	public String getPathRelative();

}
