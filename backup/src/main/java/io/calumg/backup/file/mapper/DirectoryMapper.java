package io.calumg.backup.file.mapper;

import io.calumg.backup.file.BackupFile;
import io.calumg.backup.file.exception.InvalidRelativePathException;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * DirectoryMapper takes a root directory and maps out the files within, either
 * top level or recurisvely.
 * 
 * @author calum
 *
 */
public class DirectoryMapper extends Observable {

	private List<BackupFile> filesFound;
	private BackupFile root;
	private Boolean recursive;

	public DirectoryMapper(BackupFile root, Boolean recursive) {
		this.root = root;
		this.recursive = recursive;
	}

	public DirectoryMapper() {

	}

	public void setDirectory(BackupFile root, Boolean recursive) {
		this.root = root;
		this.recursive = recursive;
	}

	private void scanForFiles(BackupFile dir, Boolean recursive) {
		try {
			for (File file : dir.listFiles()) {
				if (file.canRead()) {
					if (file.isDirectory() && recursive) {
						scanForFiles(new BackupFile(file, dir.getRelativeFilePath()), recursive);
					} else if (file.isFile()) {
						BackupFile backupFile = new BackupFile(file, dir.getRelativeFilePath());
						this.setChanged();
						this.notifyObservers(backupFile.getPathRelative());
						filesFound.add(backupFile);
					}
				}
			}
		} catch (InvalidRelativePathException e) {
			e.printStackTrace();
		}
	}

	public List<BackupFile> getMappedFiles() {
		this.filesFound = new ArrayList<>();
		this.scanForFiles(root, recursive);
		return this.filesFound;
	}
}
