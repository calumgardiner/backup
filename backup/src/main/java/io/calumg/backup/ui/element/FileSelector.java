package io.calumg.backup.ui.element;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

public class FileSelector extends HBox {

	private TextField fileField;
	private Button openButton;

	public FileSelector(String prompt, String buttonLabel) {
		this.fileField = new TextField("");
		this.fileField.setPromptText(prompt);
		this.openButton = new Button(buttonLabel);
		this.getChildren().add(fileField);
		this.getChildren().add(openButton);
		HBox.setHgrow(fileField, Priority.ALWAYS);
	}

	public void setEditable(boolean editable) {
		this.fileField.setEditable(editable);
	}

	public void setFileText(String text) {
		this.fileField.setText(text);
	}

	public String getFile() {
		return this.fileField.getText();
	}

	public void setOnAction(EventHandler<ActionEvent> event) {
		this.openButton.setOnAction(event);
	}

}
