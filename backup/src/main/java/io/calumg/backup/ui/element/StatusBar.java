package io.calumg.backup.ui.element;

import io.calumg.backup.ui.observer.StringObserver;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class StatusBar extends StackPane implements StringObserver {

	private ProgressBar progressBar;
	private Pane root;
	private Label text;

	public StatusBar(Pane root) {
		this.root = root;
		this.progressBar = new ProgressBar(0);
		this.text = new Label();
		progressBar.prefWidthProperty().bind(root.widthProperty());
		this.getChildren().add(progressBar);
	}

	public void setProgress(double progress) {
		Platform.runLater(() -> this.progressBar.setProgress(progress));
	}

	public void setProgress(double progress, String text) {
		Platform.runLater(() -> {
			StatusBar.this.progressBar.setProgress(progress);
			StatusBar.this.text.setText(text);
		});
	}

	public void scan(String text) {
		Platform.runLater(() -> {
			StatusBar.this.getChildren().remove(progressBar);
			StatusBar.this.getChildren().remove(text);
			StatusBar.this.progressBar = new ProgressBar();
			progressBar.prefWidthProperty().bind(root.widthProperty());
			StatusBar.this.text.setText(text);
			StatusBar.this.getChildren().add(progressBar);
			StatusBar.this.getChildren().add(StatusBar.this.text);
		});
	}

	/**
	 * Thread safe update to the text, can be called outwith the JavaFX thread
	 * as calls runLater.
	 */
	@Override
	public void update(String s) {
		Platform.runLater(() -> {
			StatusBar.this.text.setText(s);
		});
	}

}
