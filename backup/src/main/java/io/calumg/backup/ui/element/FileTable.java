package io.calumg.backup.ui.element;

import io.calumg.backup.file.BackupFile;

import java.util.List;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;

public class FileTable extends TableView<BackupFile> {

	private ObservableList<BackupFile> data;

	private static final TableColumn<BackupFile, String> filePathColumn = new TableColumn<BackupFile, String>(
			"File Path");
	private static final TableColumn<BackupFile, Boolean> includeFile = new TableColumn<BackupFile, Boolean>(
			"Include");
	private static final TableColumn<BackupFile, String> fileStatus = new TableColumn<BackupFile, String>(
			"Status");

	static {
		// file path column setup
		filePathColumn.setCellValueFactory(new PropertyValueFactory<>("pathRelative"));
		filePathColumn.setMinWidth(400);
		filePathColumn.setResizable(true);
		filePathColumn.setEditable(false);

		// include file column setup
		includeFile.setCellValueFactory((features) -> {
			return new SimpleBooleanProperty(features.getValue().isSelected());
		});
		includeFile.setEditable(true);
		includeFile.setCellFactory(tc -> new CheckBoxTableCell<>());

		// file status column setup
		fileStatus.setCellFactory((column) -> {
			return new TableCell<BackupFile, String>() {
				@Override
				protected void updateItem(String item, boolean empty) {
					super.updateItem(item, empty);
					String text = (item == null || item.trim().isEmpty() || empty) ? "" : item;
					setText(text);
				}
			};
		});
		fileStatus.setCellValueFactory((features) -> {
			return new SimpleStringProperty(features.getValue().getStatus().toString());
		});
		fileStatus.setEditable(false);
	}

	@SuppressWarnings("unchecked")
	public FileTable(List<BackupFile> files) {
		this.getColumns().addAll(includeFile, fileStatus, filePathColumn);
		data = FXCollections.observableArrayList(files);
		this.setItems(data);
		this.setColumnResizePolicy((resize) -> true);
	}

	public List<BackupFile> getFiles() {
		return this.data;
	}

	public void setData(List<BackupFile> files) {
		this.data.clear();
		this.data.addAll(FXCollections.observableArrayList(files));
	}

	public void addData(List<BackupFile> files) {
		this.data.addAll(files);
	}

	public void clearData() {
		this.data.clear();
	}
}
