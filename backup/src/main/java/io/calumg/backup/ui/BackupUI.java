package io.calumg.backup.ui;

import io.calumg.backup.ui.pane.BackupPane;
import io.calumg.backup.ui.scene.BackupScene;
import javafx.application.Application;
import javafx.stage.Stage;

public class BackupUI extends Application {
	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Backup");
		BackupPane pane = new BackupPane(primaryStage);
		BackupScene scene = new BackupScene(pane, 600, 600);
		primaryStage.setScene(scene);
		primaryStage.show();
		pane.focusTable();
	}
}