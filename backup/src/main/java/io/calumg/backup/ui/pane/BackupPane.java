package io.calumg.backup.ui.pane;

import io.calumg.backup.Backup;
import io.calumg.backup.error.ErrorHandling;
import io.calumg.backup.file.BackupFile;
import io.calumg.backup.file.exception.InvalidRelativePathException;
import io.calumg.backup.ui.element.FileSelector;
import io.calumg.backup.ui.element.FileTable;
import io.calumg.backup.ui.element.StatusBar;
import io.calumg.backup.ui.observer.StringObserverController;

import java.io.File;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class BackupPane extends BorderPane {

	private DirectoryChooser fileChooser;
	private Stage primaryStage;
	private Backup backup;
	private FileSelector targetSelector;
	private FileSelector backupSelector;
	private StatusBar statusBar;
	private FileTable fileTable;
	private Button analyseButton;
	private CheckBox recursiveCheck;
	private Button doBackupButton;
	private StringObserverController sObserverController;

	private static ExecutorService service = Executors.newCachedThreadPool();

	public BackupPane(Stage primaryStage) {
		this.primaryStage = primaryStage;
		buildPane();
	}

	private void buildPane() {
		this.setPadding(new Insets(10));
		backup = new Backup();
		fileChooser = new DirectoryChooser();
		buildFileSelectors();
		buildStatusBar();
		buildFileTable();
		this.sObserverController = new StringObserverController();
		this.sObserverController.addStringObserver(statusBar);
		this.backup.addObserver(sObserverController);
	}

	private void buildFileTable() {
		BorderPane tableWrapper = new BorderPane();
		tableWrapper.setPadding(new Insets(10));
		this.fileTable = new FileTable(Collections.<BackupFile> emptyList());
		tableWrapper.setCenter(fileTable);
		doBackupButton = new Button("Backup");
		doBackupButton.setDisable(true);
		doBackupButton.setMaxWidth(Double.MAX_VALUE);
		doBackupButton.setMinHeight(50d);
		BorderPane.setMargin(doBackupButton, new Insets(10, 0, 0, 0));
		tableWrapper.setBottom(doBackupButton);
		this.setCenter(tableWrapper);
	}

	private void buildStatusBar() {
		this.statusBar = new StatusBar(this);
		this.setBottom(statusBar);
	}

	private void buildFileSelectors() {
		BorderPane topWrapper = new BorderPane();
		// Build and add the selector ui elements
		targetSelector = new FileSelector("Select target directory...", "...");
		backupSelector = new FileSelector("Select backup directory...", "...");
		targetSelector.setEditable(false);
		backupSelector.setEditable(false);
		VBox selectors = new VBox();
		VBox.setMargin(targetSelector, new Insets(0, 0, 5, 0));
		selectors.getChildren().add(targetSelector);
		selectors.getChildren().add(backupSelector);
		topWrapper.setCenter(selectors);
		// Build and add the analyse button
		analyseButton = new Button("Analyse");
		analyseButton.setDisable(true);
		analyseButton.setMaxHeight(Double.MAX_VALUE);
		topWrapper.setRight(analyseButton);
		BorderPane.setMargin(analyseButton, new Insets(0, 0, 0, 5));
		// Build and add the recursive checkbox
		recursiveCheck = new CheckBox("Backup Recusively");
		recursiveCheck.setSelected(true);
		BorderPane.setMargin(recursiveCheck, new Insets(5, 0, 0, 0));
		topWrapper.setBottom(recursiveCheck);
		this.setTop(topWrapper);
		setSelectorOnActions();
		setAnalyseOnActions();
	}

	private void setAnalyseOnActions() {
		// Deal with button handler
		analyseButton.setOnAction((event) -> {
			try {
				toggleAnalyseButtons();
				this.fileTable.clearData();
				this.statusBar.scan("Analysing selected directories...");
				service.execute(() -> {
					backup.scanDirectories(recursiveCheck.isSelected());
					fileTable.setData(backup.getTargetFiles());
					toggleAnalyseButtons();
					statusBar.setProgress(1d, "Analysis Complete.");
				});
			} catch (Exception e) {
				toggleAnalyseButtons();
				ErrorHandling.displayErrorDialog("Error Analysing", e);
				e.printStackTrace();
			}
		});
	}

	private void setSelectorOnActions() {
		// Deal with handlers
		targetSelector.setOnAction((event) -> {
			chooseTargetDirectory();
			checkReady();
		});
		backupSelector.setOnAction((event) -> {
			chooseBackupDirectory();
			checkReady();
		});
	}

	private void checkReady() {
		if (backup.directoriesSet()) {
			analyseButton.setDisable(false);
		} else {
			analyseButton.setDisable(true);
		}
	}

	private void chooseTargetDirectory() {
		fileChooser.setTitle("Select directory to backup");
		File targetDir = fileChooser.showDialog(primaryStage);
		if (targetDir != null) {
			try {
				backup.setTargetDir(targetDir.getAbsolutePath());
				targetSelector.setFileText(targetDir.getAbsolutePath());
			} catch (InvalidRelativePathException e) {
				ErrorHandling.displayErrorDialog("Couldn't set the target directory.", e);
			}
		}
	}

	private void chooseBackupDirectory() {
		fileChooser.setTitle("Select directory to backup to");
		File backupDir = fileChooser.showDialog(primaryStage);
		if (backupDir != null) {
			try {
				backup.setBackupDir(backupDir.getAbsolutePath());
				backupSelector.setFileText(backupDir.getAbsolutePath());
			} catch (InvalidRelativePathException e) {
				ErrorHandling.displayErrorDialog("Couldn't set the backup directory.", e);
			}
		}
	}

	public void focusTable() {
		this.fileTable.requestFocus();
	}

	private void toggleAnalyseButtons() {
		this.analyseButton.setDisable(!this.analyseButton.isDisabled());
		this.recursiveCheck.setDisable(!this.recursiveCheck.isDisabled());
	}
}
