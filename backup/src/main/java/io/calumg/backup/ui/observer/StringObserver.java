package io.calumg.backup.ui.observer;

public interface StringObserver {
	
	public void update(String s);

}
