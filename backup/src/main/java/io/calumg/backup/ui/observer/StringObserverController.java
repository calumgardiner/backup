package io.calumg.backup.ui.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class StringObserverController implements Observer {

	private List<StringObserver> observers;

	public StringObserverController() {
		this.observers = new ArrayList<>();
	}

	public void addStringObserver(StringObserver obs) {
		this.observers.add(obs);
	}

	public void removeStringObserver(StringObserver obs) {
		this.observers.remove(obs);
	}

	@Override
	public void update(Observable o, Object arg) {
		if (arg instanceof String) {
			for (StringObserver obs : observers) {
				obs.update((String) arg);
			}
		}
	}

}
