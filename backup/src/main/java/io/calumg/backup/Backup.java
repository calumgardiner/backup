package io.calumg.backup;

import io.calumg.backup.file.BackupFile;
import io.calumg.backup.file.exception.InvalidRelativePathException;
import io.calumg.backup.file.mapper.DirectoryMapper;

import java.util.List;
import java.util.Observer;

public class Backup {

	private BackupFile targetDir;
	private BackupFile backupDir;

	private List<BackupFile> targetFiles;
	private List<BackupFile> backupFiles;

	private DirectoryMapper mapper = new DirectoryMapper();

	public Backup(String targetDirPath, String backupDirPath) throws InvalidRelativePathException {
		this.targetDir = new BackupFile(targetDirPath, targetDirPath);
		this.backupDir = new BackupFile(backupDirPath, backupDirPath);
	}

	public Backup() {

	}

	public void setTargetDir(String targetDirPath) throws InvalidRelativePathException {
		this.targetDir = new BackupFile(targetDirPath, targetDirPath);
	}

	public void setBackupDir(String backupDirPath) throws InvalidRelativePathException {
		this.backupDir = new BackupFile(backupDirPath, backupDirPath);
	}

	public boolean directoriesSet() {
		return targetDir != null && backupDir != null;
	}

	public void scanDirectories(boolean recursive) {
		mapper.setDirectory(targetDir, recursive);
		targetFiles = mapper.getMappedFiles();
		mapper.setDirectory(backupDir, recursive);
		backupFiles = mapper.getMappedFiles();
	}

	public List<BackupFile> getBackupFiles() {
		return this.backupFiles;
	}

	public List<BackupFile> getTargetFiles() {
		return this.targetFiles;
	}

	public void excludeTargetFiles(List<BackupFile> files) {
		this.targetFiles.removeAll(files);
	}

	public void analyseBackup() {
	}

	public void addObserver(Observer o) {
		this.mapper.addObserver(o);
	}

	public void deleteObserver(Observer o) {
		this.mapper.deleteObserver(o);
	}

}
