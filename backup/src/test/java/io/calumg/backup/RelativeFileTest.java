package io.calumg.backup;

import io.calumg.backup.file.exception.InvalidRelativePathException;
import io.calumg.backup.file.relative.RelativeFile;

import org.junit.Test;

@SuppressWarnings("unused")
public class RelativeFileTest {

	@Test(expected = InvalidRelativePathException.class)
	public void testValidationFailsForNonExistantPath() throws Exception {
		try {
			RelativeFile file = new RelativeFile(".", "./doesntexist");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(expected = InvalidRelativePathException.class)
	public void testValidationFailsForFile() throws Exception {
		try {
			RelativeFile file = new RelativeFile(".", "./pom.xml");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test(expected = InvalidRelativePathException.class)
	public void testValidationFailsForBadPath() throws Exception {
		try {
			RelativeFile file = new RelativeFile("./src/main/java", "./src/test");
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testValidationSuccess() throws Exception {
		RelativeFile file = new RelativeFile("./src/test/java/io/calumg/backup", "./src/test/java");
	}

}
